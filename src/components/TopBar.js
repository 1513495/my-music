import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { colors } from '../utils/styles';

class TopBar extends Component {
  constructor(props){
    super(props);
  
    this.state= {
      ishover: false,
    };
  }

  renderSignInSection = () => {
    return (
      <div className="col-lg-3 col-md-3 align-middle d-flex align-items-center justify-content-end link"
      >
        <Link className="fas fa-user-circle fa-2x link" to='/login'></Link>
        <Link className="link ml-3" to='/login'>Đăng nhập</Link>
      </div>
    );
  }

  renderUserSection = () => {
    return (
      <div className="col-lg-3 col-md-3 align-middle d-flex align-items-center">
        <i className="fas fa-user-circle fa-2x"></i>
        <span className="btn">this.props.fullName</span>
      </div>
    );
  }
  

  render() {
    return (
      <div
        style={{ backgroundColor: colors.BLACK_40 }}
        className="border-bottom border-dark"
      >
        <div className="row container mx-auto text-white">
          {/* Music Today */}
          <Link to="" className="col-lg-2 col-md-3 d-flex align-items-center link">
            <h5 className="m-auto text-warning font-weight-bold">My Music</h5>
          </Link>
          
          {/* Search */}
          <div className="col-lg-7 col-md-6 m-0 p-0">
            <div className="input-group input-group-sm m-auto p-2">
              <div className="input-group-prepend">
                <span className="input-group-text bg-dark border-right-0">
                  <i className="fas fa-search" style={{color: 'whitesmoke'}} />
                </span>
              </div>
              <input
                type="text"
                className="form-control bg-dark border-left-0"
                placeholder="Nhập tên bài hát, ca sĩ hoặc video …"
              />
            </div>
          </div>

          {/* Sign In */}
          {
            this.props.isLogin ? this.renderUserSection() : this.renderSignInSection()
          }
        </div>
      </div>
    );
  }
}

TopBar.propTypes = {
  isLogin: PropTypes.bool,
  tokenId: PropTypes.string,
  fullName: PropTypes.string,
};

const mapStateToProps = state => {
  return {
    isLogin: state.Authentication.isLogin,
    tokenId: state.Authentication.tokenId,
    fullName: state.Authentication.fullName,
  };
};

const mapDispatchToProps = () => {
  return {
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TopBar);
