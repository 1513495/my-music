import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';
import { Link } from 'react-router-dom';

import PlayerButton from './components/PlayerButton';
import * as actions from '../../actions';
import { MUSIC_PLAYER_HEIGHT, MUSIC_PLAYER_ICON } from '../../utils/const';
import { colors } from '../../utils/styles';

class MusicPlayer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentTime: 0,
    };
  }

  componentDidMount() {
    this.audio.currentTime = this.state.currentTime;
    this.audio.play();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.playingSong.id !== this.props.playingSong.id) {
      this.audio.setAttribute('src', this.props.playingSong.mp3);
    }
  }

  onClickSong = () => {
    console.log('you clicked the song');
    this.props.viewSong(this.props.playingSong.id);
  }

  onClickArtist = () => {
    console.log('you clicked the artist');
  }

  onToggle = () => {
    if (this.props.isPlaying) {
      this.audio.pause();
      this.props.onPause();
    }
    else {
      this.audio.play();
      this.props.onPlay();
    }
  }

  renderLabelTime = value => {
    let minutes = Math.floor(value / 60);
    if (minutes < 10) {
      minutes = `0${minutes}`;
    }
    let seconds = value % 60;
    if (seconds < 10) {
      seconds = `0${seconds}`;
    }

    return(
      <span className='mx-2'>
        {minutes}:{seconds}
      </span>
    );
  }

  renderSongNumber = number => {
    return(
      <div className='d-flex justify-content-center align-items-center'
        style={{
          height: '100%',
          fontWeight: '600',
        }}
      >
        {`${number} song${number > 1 ? 's' : '' }`}
      </div>
    );
  }

  render() {
    const { isPlaying, playingSong, songs } = this.props;
    const { currentTime } = this.state;
    const options = playingSong !== undefined ? playingSong.options : null;
    return (
      <div
        style={{
          ...this.props.style,
          position: 'fixed',
          bottom: 0,
          width: '100%',
          height: MUSIC_PLAYER_HEIGHT,
          backgroundColor: '#f8f4c2',
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
        }}
      >
        <div className='container'>
          <div className='row m-0 p-0'
            style={{
              height: MUSIC_PLAYER_HEIGHT,
            }}
          >
            {/* Control */}
            <div className='col-lg-3 col-md-4'
              style={{
                marginTop: (MUSIC_PLAYER_HEIGHT - MUSIC_PLAYER_ICON) / 3 * 2,
                marginBottom: (MUSIC_PLAYER_HEIGHT - MUSIC_PLAYER_ICON) / 3,
              }}
            >
              {/* step backward */}
              <div className='d-inline-block mx-auto'>
                <PlayerButton onClick={this.props.onPrevious}>
                  <i className='fa fa-step-backward'></i>
                </PlayerButton>

                {
                  isPlaying
                    ? (<PlayerButton
                      onClick={this.onToggle}
                    >
                      <i className='fa fa-2x fa-pause-circle'></i>
                    </PlayerButton>)
                    : (<PlayerButton
                      onClick={this.onToggle}
                    >
                      <i className='fa fa-2x fa-play-circle'></i>
                    </PlayerButton>)
                }

                {/* step forward */}
                <PlayerButton onClick={this.props.onNext}>
                  <i className='fa fa-step-forward'></i>
                </PlayerButton>

                {/* mode random */}
                <PlayerButton>
                  <i className='fa fa-random'></i>
                </PlayerButton>

                {/* mode repeat */}
                <PlayerButton>
                  <i className='fa fa-retweet'></i>
                </PlayerButton>
              </div>
            </div>
            {/* Display song, artist, player */}
            <div className='col-lg-7 col-md-6'>
              {/* song, artist */}
              <div className='mt-2 mb-0'>
                <h6 className='text-center text-capitalize mb-0'>
                  <Link
                    className='link-black my-0'
                    onClick={this.onClickSong}
                    to='song'
                  >{playingSong.name} </Link>
                  -
                  <span
                    className='link-black my-0'
                    onClick={this.onClickArtist}
                  > {options ? options.artist.name : null}</span>
                </h6>
              </div>
              
              {/* Audio */}
              <div className='d-flex'>
                {/* current time */}
                {this.renderLabelTime(currentTime)}

                {/* slider */}
                <div className='d-inline-block flex-grow-1 mt-2 mx-1'>
                  <Slider
                    ref={ref => this.slider = ref }
                    min={0}
                    max={playingSong.length}
                    value={currentTime}
                    defaultValue={0}
                    onChange={value => {
                      if (!this.audio.paused){
                        this.audio.pause();
                      }
                      this.setState({ currentTime: value });
                      this.audio.currentTime = value;
                    }}
                    onAfterChange={() => { this.audio.play(); }}
                    step={1}
                    activeDotStyle={{ color: colors.PRIMARY }}
                    trackStyle={{ backgroundColor: colors.PRIMARY }}
                    handleStyle={{
                      width: 16,
                      height: 16,
                      bottom: 0,
                      borderWidth: 3,
                      borderColor: colors.PRIMARY,
                    }}
                    railStyle={{ backgroundColor: colors.BLACK_30 }}
                  ></Slider>
                </div>
                {/* duration time */}
                {this.audio ? this.renderLabelTime(Math.floor(this.audio.duration)) : this.renderLabelTime(0)}
                <audio
                  ref={ref => this.audio = ref }
                  autoPlay
                  onTimeUpdate={() => {
                    const newSecond = Math.floor(this.audio.currentTime);
                    if (newSecond !== this.state.currentTime) {
                      this.setState({ currentTime: newSecond });
                    }
                  }}
                  onPlay={() =>{ this.props.onPlay(); }}
                  onPause={() =>{ this.props.onPause(); }}
                >
                  <source src={playingSong.mp3} type='audio/mpeg'></source>
                </audio>
              </div>
            </div>
            <div className='col-md-2'>
              {this.renderSongNumber(songs.length)}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

MusicPlayer.propTypes = {
  style: PropTypes.object,
  isPlaying: PropTypes.bool,
  songs: PropTypes.array,
  playingSong: PropTypes.object,
  onPause: PropTypes.func,
  onPlay: PropTypes.func,
  onNext: PropTypes.func,
  onPrevious: PropTypes.func,
  setCurrentSecond: PropTypes.func,
  viewSong: PropTypes.func,
};

const mapStateToProps = state => {
  return {
    isPlaying: state.MusicPlayer.isPlaying,
    songs: state.MusicPlayer.songs,
    playingSong: state.MusicPlayer.playingSong,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onPause: () => {
      dispatch(actions.pause());
    },
    onPlay: () => {
      dispatch(actions.play());
    },
    onNext: () => {
      dispatch(actions.nextSong());
    },
    onPrevious: () => {
      dispatch(actions.previousSong());
    },
    setCurrentSecond: second => {
      dispatch(actions.setCurrentSecond(second));
    },
    viewSong: id => {
      dispatch(actions.viewSong(id));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MusicPlayer);
