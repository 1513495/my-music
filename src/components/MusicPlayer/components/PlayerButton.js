import React, { Component } from 'react';

import PropTypes from 'prop-types';
import { MUSIC_PLAYER_ICON } from '../../../utils/const';

class PlayerButton extends Component {
  render() {
    const size = MUSIC_PLAYER_ICON;

    return (
      <div
        style={{
          display: 'inline-block',
          cursor: 'pointer',
        }}
        className='hover'
        onClick={this.props.onClick}
      >
        <div style={{
          width: size,
          height: size,
          display: 'table-cell',
          textAlign: 'center',
          verticalAlign: 'middle',
        }}>
          {this.props.children}
        </div>
      </div>
    );
  }
}

PlayerButton.propTypes = {
  children: PropTypes.element,
  onClick: PropTypes.func,
};

export default PlayerButton;