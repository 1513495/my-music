import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';

import { colors } from '../utils/styles';

class NavItem extends Component {
  render() {
    return (
      <li className="nav-item">
        <NavLink
          to={`/${this.props.to}`}
          activeStyle={{
            color: colors.PRIMARY,
            borderBottomWidth: '5px',
            borderBottomColor: colors.PRIMARY,
          }}
          style={{
            color: colors.WHITE,
          }}
          className="nav-link text-uppercase small"
          exact={this.props.to === '' ? true : false}
        >
          {this.props.content}
        </NavLink>
      </li>
    );
  }
}

NavItem.propTypes = {
  to: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
};

class MenuBar extends Component {
  render() {
    return (
      <div style={{ backgroundColor: colors.SECONDARY }}>
        <div className="container">
          <div className="row">
            <ul className="nav col-md-12 px-5">
              <NavItem content="Trang Chủ" to=""/>
              <NavItem content="BXH" to="rank"/>
              <NavItem content="Top 100" to="top100"/>
              <NavItem content="Chủ đề" to="topic"/>
              <NavItem content="Video" to="video"/>
              <NavItem content="Album" to="album"/>
              <NavItem content="Nghệ Sĩ" to="artist"/>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default MenuBar;