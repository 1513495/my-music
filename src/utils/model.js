import Axios from 'axios';

import { API_URL } from '../utils/const';

export const login = (phone, password) => {
  // phone: 0978975609, password: admin@123
  return Axios.get(API_URL + 'user.php', { params: { phone, password } });
};

export const register = (phone, password, name, gender) => {
  // phone: 0978975609, password: admin@123
  // eslint-disable-next-line
  return Axios.post(API_URL + 'user.php', { phone, password, full_name: name, sex: gender });
};

export const getSong = id => {
  return Axios.get(API_URL + 'song.php', { params: { id }});
};

export const getSongs = () => {
  return Axios.get(API_URL + 'song.php');
};

export const getsongsDES = () =>{
  return Axios.get(API_URL + 'song.php?order=view&type=DESC');
};

export const getAlbum = id => {
  return Axios.get(API_URL + 'album.php', {params: { id }});
};

export const getArtists = () => {
  return Axios.get(API_URL + 'singer.php');
};

export const getAlbums = () => {
  return Axios.get(API_URL + 'album.php');
};