import * as types from '../actions/types';

const defaultState = {
  isLogin: false,
  tokenId: '',
  fullName: '',
};

const Authentication = (state = defaultState, action) => {
  switch (action.type){
    case types.LOGIN: {
      const { tokenId, fullName } = action;
      return {
        isLogin: true,
        tokenId: tokenId,
        fullName: fullName,
      };
    }
    case types.REGISTER: {
      const { name, phone, gender, password } = action;
      return {
        isLogin: true,
        tokenId: tokenId,
        fullName: fullName,
      };
    }
    
    default: return state;
  }
};

export default Authentication;