import { combineReducers } from 'redux';
import Authentication from './Authentication';
import MusicPlayer from './MusicPlayer';
import Common from './Common';

const myReducer = combineReducers({
  Authentication,
  MusicPlayer,
  Common,
});

export default myReducer;