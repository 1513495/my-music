import * as types from '../actions/types';

const defaultState = {
  currentSongId: '',
  currentAlbumId: '',
};

const Common = (state = defaultState, action) => {
  switch (action.type){
    case types.WATCH_SONG: {
      return {
        currentSongId: action.id,
      };
    }
    case types.VIEW_SONG: {
      return {
        currentSongId: action.id,
      };
    }
    case types.VIEW_ALBUM: {
      return {
        currentAlbumId: action.id,
      };
    }
    
    default: return state;
  }
};

export default Common;