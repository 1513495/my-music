import * as types from '../actions/types';

const Mode = {
  shuffle: 0,
  circle: 1,
  repeat: 2,
};

const defaultState = {
  isEmpty: true,
  songs: [],
  playingSong: null,
  currentIndex: 0,
  isPlaying: false,
  mode: Mode.circle,
  currentTime: 0,
};

const MusicPlayer = (state = defaultState, action) => {
  switch (action.type){
    case types.ADD_SONG: {
      const { song } = action;
      
      let ids = state.songs.map(song => song.id);
      if (ids.includes(song.id)) {
        return state;
      }
      return {
        ...state,
        songs: [
          ...state.songs,
          song,
        ],
        isEmpty: false,
        playingSong: song,
        currentIndex: state.songs.length,
      };
    }
    case types.ADD_SONGS: {
      const { songs } = action;
      
      songs.map(song => {
        if (!state.songs.map(song => song.id)
          .includes(song.id)) {
          state = {
            ...state,
            songs: [
              ...state.songs,
              song,
            ],
          };
        }
      });

      const length = state.songs.length;
      state.currentIndex = length - 1;
      state.playingSong = state.songs[state.currentIndex];
      state.isEmpty = false;
      return state;
    }
    case types.PAUSE: {
      return {
        ...state,
        isPlaying: false,
      };
    }
    case types.PLAY: {
      return {
        ...state,
        isPlaying: true,
      };
    }
    case types.NEXT_SONG: {
      let nextIndex = state.currentIndex + 1;
      if (nextIndex >= state.songs.length) {
        nextIndex = 0;
      }
      return {
        ...state,
        currentIndex: nextIndex,
        playingSong: state.songs[state.currentIndex],
      };
    }
    case types.PREVIOUS_SONG: {
      let nextIndex = state.currentIndex - 1;
      if (nextIndex === -1) {
        nextIndex = state.songs.length;
      }
      return {
        ...state,
        currentIndex: nextIndex,
        playingSong: state.songs[state.currentIndex],
      };
    }

    default: return state;
  }
};

export default MusicPlayer;