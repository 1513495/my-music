// Authentication
export const LOGIN = 'LOGIN';
export const REGISTER = 'REGISTER';


// Music Player
export const ADD_SONG = 'ADD_SONG';
export const ADD_SONGS = 'ADD_SONGS';
export const PAUSE = 'PAUSE';
export const PLAY = 'PLAY';
export const NEXT_SONG = 'NEXT_SONG';
export const PREVIOUS_SONG = 'PREVIOUS_SONG';
export const SHUFFLE = 'SHUFFLE';
export const REPEAT = 'REPEAT';

// Common
export const WATCH_SONG = 'WATCH_SONG';
export const VIEW_ALBUM = 'VIEW_ALBUM';
export const VIEW_SONG = 'VIEW_SONG';


