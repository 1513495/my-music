import * as types from './types.js';

export const login = (tokenId, fullName) => {
  return {
    type: types.LOGIN,
    tokenId,
    fullName,
  };
};