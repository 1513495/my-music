import * as types from './types.js';

export const addSong = song => {
  return {
    type: types.ADD_SONG,
    song,
  };
};

export const addSongs = songs => {
  return {
    type: types.ADD_SONGS,
    songs,
  };
};


export const pause = () => {
  return {
    type: types.PAUSE,
  };
};

export const play = () => {
  return {
    type: types.PLAY,
  };
};

export const nextSong = () => {
  return {
    type: types.NEXT_SONG,
  };
};

export const previousSong = () => {
  return {
    type: types.PREVIOUS_SONG,
  };
};

export const shuffleSongs = () => {
  return {
    type: types.SHUFFLE,
  };
};

export const repeat = () => {
  return {
    type: types.REPEAT,
  };
};
