import * as types from './types.js';

export const viewAlbum = id => {
  return {
    type: types.VIEW_ALBUM,
    id,
  };
};

export const viewSong = id => {
  return {
    type: types.VIEW_SONG,
    id,
  };
};