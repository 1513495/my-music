import React, { Component } from 'react';
import { colors } from '../../utils/styles';
import { Link } from 'react-router-dom';

import { register } from '../../utils/model';

const styles = {
  input: {
    marginTop: 10,
  },
  button: {
    backgroundColor: colors.PRIMARY,
    borderRadius: 10,
    borderWidth: 0,
    fontWeight: 'bold',
    marginTop: 10,
  },
  heading: {
    marginTop: 10,
    marginBottom: 0,
    color: colors.PRIMARY,
    fontWeight: 'bold',
  },
};

class Register extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name:'',
      gender:'',
      phone: '',
      password: '',
    };
  }

  onChange = event => {
    this.setState({ [event.target.name]: event.target.value});
  };

  onClick = () =>{
    const{ phone, password, name, gender } = this.state;
    register(phone, password, name, gender)
      .then(response => {
        console.log(JSON.stringify(response));
      })
      .catch(err => {
        console.log(JSON.stringify(err));
      });
    
  }

  render() {
    return (
      <div className="container">
        { this.state.isDisplayAlert ? this.renderAlertError() : null }

        <div className="row">
          <div className="col-sm-9 col-md-7 col-lg-5 mx-auto">
            <div className="card card-signin my-5"
              style={{ borderRadius: 15 }}
            >
              <div className='card-heading'>
                <h3 style={styles.heading} className='text-center'>REGISTER</h3>
              </div>
              <div className="card-body">
                <div className="form-label-group">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Full name"
                    style={styles.input}
                    required
                    autoFocus
                    onChange={this.onChange}
                    name='name'
                  />
                </div>

                <div className="form-label-group">
                  <input
                    type="number"
                    className="form-control"
                    placeholder="phone"
                    style={styles.input}
                    required
                    autoFocus
                    onChange={this.onChange}
                    name='phone'
                  />
                </div>

                <label htmlFor="email" className='mt-1'>Gender:</label>
                <div className="form-check">
                  <label className="form-check-label">
                    <input type="radio" className="form-check-input" name="gender" onChange={this.onChange} value="male"/>
                    Male
                  </label>
                </div>

                <div className="form-check">
                  <label className="form-check-label">
                    <input type="radio" className="form-check-input" name="gender" onChange={this.onChange} value="female"/>
                    Female
                  </label>
                </div>

                <div className="form-label-group">
                  <input
                    type="password"
                    className="form-control"
                    placeholder="Password"
                    required
                    style={styles.input}
                    onChange={this.onChange}
                    name='password'
                    onKeyPress = {e => e.key === 'Enter' ? this.onClick() : null}
                  />
                </div>
                <button className="btn btn-lg btn-primary btn-block text-uppercase" type="submit"
                  style={styles.button}
                  onClick={this.onClick}
                >Register</button>
                <Link className="btn btn-lg btn-primary btn-block text-uppercase" type="submit"
                  style={styles.button}
                  to="/login"
                >Sign in</Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Register;
