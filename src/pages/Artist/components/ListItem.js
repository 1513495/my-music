import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const style = {
  divImage: {
    width: 200,
    height: 200,
    borderRadius: 100,
    overflow: 'hidden',
    display: 'inline-block',
  },
  icon: {
    width: 40,
    height: 40,
  },
  title: {
    textTransform: 'capitalize',
    fontSize: 22,
    fontWeight: 'bold',
    marginLeft: 20,
  },
};

class Item extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isHover: false,
    };
  }
  render() {
    return(
      <div
        className="col-lg-3 col-md-4 col-sm-6 align-items-center justify-content-center mb-3"
        style={{ textAlign: 'center' }}
      >
        {/* avatar */}
        <div style={style.divImage}>
          <Link to="artist">
            <img
              src={this.props.url}
              alt=""
              className="mx-auto artist-avatar"
            />
          </Link>
        </div>

        {/* artist name */}
        <div className="mb-1">
          <Link to="artist" className="link">
            <span style={{ textTransform: 'capitalize', fontSize: 18, fontWeight: 'bold' }}>
              {this.props.name}
            </span>
          </Link>
        </div>
      </div>
    );
  }
}

Item.propTypes = {
  url: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
};

class ListItem extends Component {
  render() {
    return (
      <div className="row py-2 my-2">
        <div className="col-lg-12">
          {/* title */}
          <Link to="artist" className="link">
            <span style={style.title}>
              {this.props.title}
              &nbsp;
              <i className="fas fa-chevron-right"></i>
            </span>
          </Link>

          {/* list artists */}
          <div className="my-3 row">
            {
              this.props.artists.map(artist => (
                <Item key={artist.id} url={artist.options.image} name={artist.full_name}/>
              ))
            }
          </div>
        </div>
      </div>
    );
  }
}

ListItem.propTypes = {
  title: PropTypes.string.isRequired,
  artists: PropTypes.array.isRequired,
};

export default ListItem;