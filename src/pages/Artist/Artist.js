import React, { Component } from 'react';

import ListItem from './components/ListItem';
import { getArtists } from '../../utils/model';

class Artist extends Component {
  constructor(props){
    super(props);

    this.state = {
      artists: [],
    };
  }

  componentDidMount(){
    getArtists()
      .then(response => {
        const artists = response.data;

        this.setState({ artists });
      })
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    const { artists } = this.state;

    return (
      <div className="pb-5">
        <div className="container text-white bg-black p-0">
          <div className="px-5 pt-4 pb-2">
            <ListItem title="việt nam" artists={artists.slice(0, 8)}/>
            <ListItem title="hàn quốc" artists={artists.slice(0, 8)}/>
            <ListItem title="âu mỹ" artists={artists.slice(0, 8)}/>
          </div>
        </div>
      </div>
    );
  }
}

export default Artist;
