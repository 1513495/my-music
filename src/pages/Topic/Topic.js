import React, { Component } from 'react';

import ListItem from './components/ListItem.js';

class Topic extends Component {
  render() {
    return (
      <div className="pb-5">
        <div className="container text-white bg-black p-0">
          <div className="px-5 pt-4">
            <h4 className="text-capitalize">nhạc theo chủ đề</h4>
            <p className="text-gray">
            Nhạc hay nhất theo chủ đề được chọn lọc kỹ càng và cập nhật liên tục. Bạn đang vui hay buồn, đang tiệc tùng hay trên đường đi phượt, lúc thức dậy hay đang vào giấc ngủ ... những gì bạn cần đều có ở đây!
            </p>
          </div>

          {/* <hr className="m-0 p-0 bg-secondary"/> */}

          <ListItem />
        </div>
      </div>
    );
  }
}

export default Topic;
