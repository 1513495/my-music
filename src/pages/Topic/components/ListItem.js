import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import { colors } from '../../../utils/styles';

const style = {
  image: {
    width: 210,
    height: 118,
    transition: 'all 500ms ease-out',
  },
  divTitle: {
    width: 210,
    height: 118,
    position: 'absolute',
    textDecoration: 'none',
    top: 0,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    transition: 'all 500ms ease-out',
  },
  title: {
    textTransform: 'capitalize',
    fontWeight: 'bold',
    position: 'absolute',
    transition: 'all 500ms ease-out',
  },
};

class Item extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isHover: false,
    };
  }
  render() {
    const isHover = this.state.isHover;
    return(
      <div
        className="col-lg-3 col-md-4 col-sm-6 align-items-center justify-content-center mb-3"
        style={{ textAlign: 'center' }}
      >
        {/* album cover */}
        <div style={{
          overflow: 'hidden',
          display: 'inline-block',
        }}>
          <img
            src={this.props.url}
            alt=""
            className="mx-auto"
            style={{
              ...style.image,
              transform: isHover ? 'scale(1.1, 1.1)' : '',
            }}
          />

          {/* title */}
          <Link to="album" className="link"
            style={style.divTitle}
            onMouseEnter={() => { this.setState({ isHover: true }); }}
            onMouseLeave={() => { this.setState({ isHover: false }); }}
          >
            <span style={{
              ...style.title,
              top: isHover ? 50 : 60,
              color: isHover ? colors.PRIMARY : colors.WHITE,
            }}>
              {this.props.title}
            </span>
          </Link>
        </div>
      </div>
    );
  }
}

Item.propTypes = {
  url: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
};

class ListItem extends Component {
  render() {
    const data = [
      {
        url: 'https://goo.gl/2zogTQ',
        title: 'Những Bài Hit',
      },
      {
        url: 'https://photo-zmp3.zadn.vn/covers/1/3/1301c274e77b9fc0e99a4b258eed6d52_1520571632.jpg',
        title: 'Hôm Nay Nghe Gì',
      },
      {
        url: 'https://photo-zmp3.zadn.vn/cover/1/2/e/f/12ef2fb318b9d94721e59bcb8e2c25d7.jpg',
        title: 'Happy',
      },
      {
        url: 'https://photo-zmp3.zadn.vn/cover/b/3/1/2/b312802cc30c610a0b1e096242cfe3eb.jpg',
        title: 'Indie & Underground',
      },
      {
        url: 'https://photo-zmp3.zadn.vn/cover/1/a/8/2/1a822c5d35b6b455f3ce7f02b475f2a3.jpg',
        title: 'Thay Lời Muốn Nói',
      },
      {
        url: 'https://photo-zmp3.zadn.vn/cover/5/4/e/7/54e72fa3192be550632914af389f3958.jpg',
        title: 'Thế Giới V-pop',
      },
      {
        url: 'https://photo-zmp3.zadn.vn/banner/2/a/2ac9d9aa479519e1724db5b860373578_1499168345.jpg',
        title: 'Love',
      },
      {
        url: 'https://photo-zmp3.zadn.vn/banner/d/1/d1c2738deec7efd1942a3027a1c436b0_1499168870.jpg',
        title: 'Nhạc Hot',
      },
      {
        url: 'https://photo-zmp3.zadn.vn/banner/c/5/c5360ad3d2e28b5bb5f0d0930a6a6a6f_1499168316.jpg',
        title: 'Acoustic',
      },
      {
        url: 'https://photo-zmp3.zadn.vn/banner/7/0/702ed1b745f1b326f4fc07e8b60afea4_1499168580.jpg',
        title: 'Buồn',
      },
      {
        url: 'https://photo-zmp3.zadn.vn/cover/5/d/e/0/5de04edcf8f1008a9f20bc658e0af41d.jpg',
        title: 'Ca Sĩ Âu Mỹ',
      },
      {
        url: 'https://photo-zmp3.zadn.vn/cover/5/2/8/d/528df9d8c6163f0fc15521584e959263.jpg',
        title: 'Nhạc Sĩ Việt Nam',
      },
      {
        url: 'https://photo-zmp3.zadn.vn/cover/6/5/2/2/652209152e43db50a7279d9574cdda20.jpg',
        title: 'Motivation',
      },
      {
        url: 'https://photo-zmp3.zadn.vn/banner/0/c/0c422740077e31321572a2d62d13e13d_1499168268.jpg',
        title: 'Làm Việc',
      },
      {
        url: 'https://photo-zmp3.zadn.vn/cover/8/0/7/6/80768e9ab4c83585182bf8199c69405e.jpg',
        title: 'Ca Sĩ Hàn Quốc',
      },
      {
        url: 'https://photo-zmp3.zadn.vn/covers/4/b/4bd01bd755b5cf16681a82d15e77caea_1515483012.jpg',
        title: 'Radio MP3',
      },
      {
        url: 'https://photo-zmp3.zadn.vn/cover/0/b/e/c/0becb5c8082d040cacaf7682b008937a.jpg',
        title: 'Nhạc Việt Bất Hủ',
      },
      {
        url: 'https://photo-zmp3.zadn.vn/banner/5/a/5a196906109e857d48370469be7acc42_1474890427.jpg',
        title: 'Ca Sĩ Việt Nam',
      },
      {
        url: 'https://photo-zmp3.zadn.vn/banner/6/d/6d22f54a6efbd739047abc976acd5dbb_1499168283.jpg',
        title: 'Bất Hủ Âu Mỹ',
      },
      {
        url: 'https://photo-zmp3.zadn.vn/covers/4/b/4bd01bd755b5cf16681a82d15e77caea_1515485857.jpg',
        title: 'Top 100',
      },
    ];
    return(
      <div className="container">
        {/* list */}
        <div className="row py-2 my-2">
          <div className="m-3 row">
            {
              data.map((value, idx) => (
                <Item key={idx} url={value.url} title={value.title}/>
              ))
            }
          </div>
        </div>
      </div>
    );
  }
}

export default ListItem;