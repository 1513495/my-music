import React, { Component } from 'react';

import ListItem from './components/ListItem';
import { getAlbums } from '../../utils/model';

class Top100 extends Component {
  constructor(props){
    super(props);

    this.state = {
      albums: [],
    };
  }

  componentDidMount(){
    getAlbums()
      .then(response => {
        this.setState({ albums: response.data });
      })
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    const { albums } = this.state;

    return (
      <div className="pb-5">
        <div className="container text-white bg-black p-0">
          <div className="px-5 pt-4 pb-2">
            <h4>Top 100</h4>
            <p className="text-gray">
            TOP 100 là danh sách 100 ca khúc hot nhất hiện tại của từng thể loại nhạc, được hệ thống tự động đề xuất dựa trên thông tin số liệu lượt nghe và lượt chia sẻ của từng bài hát. Dữ liệu sẽ được lấy trong 30 ngày gần nhất và được cập nhật liên tục.
            </p>
          </div>
          <hr className="m-0 p-0 bg-secondary"/>
          <div className="px-5 pt-4 pb-2">
            <ListItem title="Nổi bật" albums={albums.slice(0, 4)}/>
            <ListItem title="việt nam" albums={albums.slice(0, 8)}/>
            <ListItem title="âu mỹ" albums={albums.slice(0, 8)}/>
          </div>
        </div>
      </div>
    );
  }
}

export default Top100;
