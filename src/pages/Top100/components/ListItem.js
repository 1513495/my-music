import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import * as actions from '../../../actions';

import '../../../utils/mymusic.scss';
import { colors } from '../../../utils/styles';
import { getAlbum } from '../../../utils/model';

const style = {
  divIcon: {
    position: 'absolute',
    textDecoration: 'none',
    top: 0,
    opacity: 0,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    transition: 'all 500ms ease-out',
  },
  icon: {
    color: colors.WHITE,
    backgroundColor: colors.PRIMARY,
    padding: 18,
    borderRadius: '50%',
  },
};

class Item extends Component {
  constructor(props) {
    super(props);

    this.state = { isHover: false };
  }
  render() {
    const { album, history } = this.props;
    return(
      <div
        className="col-lg-3 col-md-4 col-sm-6 align-items-center justify-content-center mb-3"
        style={{ textAlign: 'center' }}
      >
        {/* album cover */}
        <div
          style={{ overflow: 'hidden', display: 'inline-block' }}
          className="card-200"
        >
          <img
            src={album.options.cover}
            alt=""
            className="card-200"
            style={{
              transition: 'all 500ms ease-out',
              transform: this.state.isHover ? 'scale(1.1, 1.1)' : '',
            }}
          />
          <div
            style={{
              ...style.divIcon,
              opacity: this.state.isHover ? 1 : 0,
              cursor: 'pointer',
            }}
            className="card-200"
            onMouseEnter={() => { this.setState({ isHover: true }); }}
            onMouseLeave={() => { this.setState({ isHover: false }); }}
            onClick={() => {
              console.log(album.id);
              getAlbum(album.id)
                .then(response => {
                  console.log(response.data);
                  const songs = response.data.songs;
                  
                  this.props.addSongs(songs);
                })
                .catch(err => {
                  console.log(err);
                });
            }}
          >
            <i className="fas fa-play" style={style.icon}></i>
            {/* </div> */}
          </div>
          
        </div>

        {/* Album name */}
        <div className="mt-2">
          <Link className="link"
            to='/albumDetail'
            onClick={() => {
              this.props.viewAlbum(album.id);
            }}
          >
            <span style={{ textTransform: 'capitalize' }}>
              {album.name}
            </span>
          </Link>
        </div>
      </div>
    );
  }
}

Item.propTypes = {
  album: PropTypes.object.isRequired,
  viewAlbum: PropTypes.func.isRequired,
  history: PropTypes.object,
  addSongs: PropTypes.func,
};

class ListItem extends Component {
  render() {
    const { albums } = this.props;

    return(
      <div>
        <h5 className="text-capitalize">{this.props.title}</h5>
        
        {/* list */}
        <div className="row py-2 my-2">
          <div className="my-3 row">
            {
              albums.map(album => (
                <Item key={album.id} album={album} viewAlbum={this.props.viewAlbum}
                  addSongs={this.props.addSongs}
                />
              ))
            }
          </div>
        </div>
      </div>
    );
  }
}

ListItem.propTypes = {
  title: PropTypes.string.isRequired,
  albums: PropTypes.array.isRequired,
  viewAlbum: PropTypes.func,
  addSongs: PropTypes.func,
};

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    viewAlbum: id => {
      dispatch(actions.viewAlbum(id));
    },
    addSongs: songs => {
      dispatch(actions.addSongs(songs));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ListItem);