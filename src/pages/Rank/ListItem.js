import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import * as actions from '../../actions';

const style = {
  icon: {
    width: 40,
    height: 40,
  },
};

class Item extends Component {
  onClick = () => {
    this.props.addSong(this.props.song);
  }

  render() {
    const { song } = this.props;
    return (
      <tr>
        <td>
          <h4 className="d-inline">{this.props.index}</h4>
        </td>

        <td>
          <img
            className="img-fluid rounded"
            src={song.image}
            style={style.icon}
            alt={song.name}
          />
          <Link className="d-inline large text-capitalize ml-2 link"
            style={{ cursor: 'pointer' }}
            to='song'
            onClick={() => {
              this.props.viewSong(song.id);
            }}
          >{song.name}</Link>
        </td>

        <td>
          <button type="button" className="btn btn-outline-warning float-right"
            onClick={this.onClick}
          >Play</button>
        </td>
      </tr>
    );
  }
}

Item.propTypes = {
  index: PropTypes.number.isRequired,  
  song: PropTypes.object.isRequired,
  addSong: PropTypes.func.isRequired,
  viewSong: PropTypes.func.isRequired,
};

class ListItem extends Component {
  render() {
    return (
      <table className="table table-striped ">
        <tbody>
          {
            this.props.data.map((song, idx) => (
              <Item key={song.id} index={idx + 1} song={song} addSong={this.props.addSong} viewSong={this.props.viewSong}/>
            ))
          }
        </tbody>
      </table>
    );
  }
}

ListItem.propTypes = {
  data: PropTypes.array.isRequired,
  addSong: PropTypes.func,
  viewSong: PropTypes.func,
};

const mapStateToProps = state => {
  return {
    isLogin: state.Authentication.isLogin,
    tokenId: state.Authentication.tokenId,
    fullName: state.Authentication.fullName,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onLogin: (tokenId, fullName) => {
      dispatch(actions.login(tokenId, fullName));
    },
    addSong: song => {
      dispatch(actions.addSong(song));
    },
    viewSong: id => {
      dispatch(actions.viewSong(id));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ListItem);
