import React, { Component } from 'react';

import ListItem from './ListItem';
import { getsongsDES } from '../../utils/model';


class Rank extends Component {
  
  constructor(props) {
    super(props);

    this.state = {
      songs: [],
    };

  }

  componentDidMount() {
    getsongsDES()
      .then(response => {
        let arr = response.data;
        for (let i = 0; i < arr.length; i++) {
          // console.log(arr.length);
          this.setState({
            songs: [
              ...this.state.songs,
              arr[i],
            ],
          });
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  render() {
    return (
      <div className="pb-5">
        <div className="container text-white bg-black p-0">
          <div className="px-5 pt-4">
            <h4 className="text-capitalize">Bảng xếp hạng tháng</h4>
            <ListItem data={
              this.state.songs
            }
            />
          </div>
        </div>
      </div>
    );
  }
}

export default Rank;
