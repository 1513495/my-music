import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { colors } from '../../utils/styles';
import * as actions from '../../actions';
import { getSong } from '../../utils/model';

class CoverSongImage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isPlaying: false,
      isHover: false,
    };
  }

  render() {
    return (
      <div style={{
        width: 160,
        height: 160,
        display: 'inline-block',
        overflow: 'hidden',
      }}>
        <img
          src={this.props.url}
          alt=""
          style={{
            width: 160,
            height: 160,
            transition: 'all 500ms ease-out',
            transform: this.state.isHover ? 'scale(1.1, 1.1)' : '',
          }}
        />
        <div
          style={{
            position: 'absolute',
            textDecoration: 'none',
            top: 0,
            width: 160,
            height: 160,
            cursor: 'pointer',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            transition: 'all 500ms ease-out',
          }}
          onClick={() => {
            this.setState({
              isPlaying: !this.state.isPlaying,
            });
          }}
          onMouseEnter={() => { this.setState({ isHover: true }); }}
          onMouseLeave={() => { this.setState({ isHover: false }); }}
        >
          <i
            className={this.state.isPlaying ? 'fas fa-pause' : 'fas fa-play'}
            style={{
              color: colors.PRIMARY,
              backgroundColor: colors.WHITE,
              padding: 18,
              borderRadius: '50%',
            }}></i>
        </div>
      </div>
    );
  }
}

CoverSongImage.propTypes = {
  url: PropTypes.string.isRequired,
};

class Song extends Component {
  constructor(props) {
    super(props);

    this.state = {
      song: {},
    };
  }

  componentDidMount(){
    getSong(this.props.songId)
      .then(response => {
        this.setState({ song: response.data });

        console.log(this.state);
      })
      .catch(error => {
        console.log(error);
      });
  }

  componentDidUpdate(prevProps) {
    if (prevProps.songId !== this.props.songId) {
      getSong(this.props.songId)
        .then(response => {
          this.setState({ song: response.data });

          console.log(this.state);
        })
        .catch(error => {
          console.log(error);
        });
    }
  }

  render() {
    const { song } = this.state;
    const singer = song.singers !== undefined ? song.singers[0] : {};

    return (
      <div className="pb-5">
        <div className="container text-white bg-black p-0">
          <div
            style={{
              height: 220,
              backgroundColor: colors.BLACK_40,
            }}
            className='px-5 py-3'
          >
            <div className="row align-items-center h-100">
              <div className="col-lg-3 col-md-4 col-sm-5 d-none d-sm-block d-md-block d-lg-block d-xl-block px-0">
                <CoverSongImage url={song.image}/>
              </div>

              <div
                style={{
                }}
                className="col-lg-6 col-md-8 col-sm-7 px-0"
              >
                <h5 className="my-0">{song.name}</h5>
                <span>{song.singers !== undefined ?song.singers[0].full_name : ''}</span>
              </div>

              <div
                className="col-lg-3 d-none d-lg-block d-xl-block"
                style={{ height: '100%', verticalAlign: 'text-bottom' }}
              >
                <div style={{ right: 20, bottom: 10, position: 'absolute' }}>
                  {/* listenings */}
                  <span>
                    <i className="fas fa-play"></i>
                    <span className="font-weight-bold ml-2">{song.view}</span>
                  </span>
                </div>
              </div>
            </div>
          </div>
        
          <div className="container py-4 row">
            <div className="col-md-8 mx-2">
              {/* Artist */}
              <div className='my-2'>
                <img
                  src={song.singers ? song.singers[0].options.image : ''}
                  alt=""
                  style={{ width: 50, height: 50, borderRadius: 25 }}
                />
                <span
                  className="font-weight-bold ml-2"
                  style={{ cursor: 'pointer' }}
                >{singer.full_name}</span>
              </div>
              {/* Lyric */}
              <div>
                {song.description}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Song.propTypes = {
  songId: PropTypes.string,
};

const mapStateToProps = state => {
  return {
    songId: state.Common.currentSongId,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onLogin: (tokenId, fullName) => {
      dispatch(actions.login(tokenId, fullName));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Song);
