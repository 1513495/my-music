import React, { Component } from 'react';

class Album extends Component {
  render() {
    return (
      <div className="pt-3 pb-5">
        <div className="container text-white">
          <h4>Album</h4>
          <p>
          Ipsum blandit justo aute ex pede curabitur aliqua. Per adipiscing porta sed consectetuer vivamus pellentesque maecenas rutrum blandit. Pretium cum proident aliquet primis aliquip porta incididunt. Mauris porttitor qui massa urna fringilla venenatis. Dolor ante amet nostra cubilia scelerisque nisi. Penatibus erat fusce fames ex massa adipiscing. Convallis laboris tellus varius velit lectus non fermentum. Phasellus hendrerit imperdiet dolor facilisis. Phasellus amet ridiculus nec ornare ligula porttitor fugiat. Deserunt ullamco libero sem arcu fusce mattis voluptate eiusmod aenean. Cupidatat et lobortis morbi minim ante nam feugiat dolor veniam.
          </p>

          <p>
          Dapibus tincidunt nibh sit sagittis sociis est. Morbi sodales culpa aptent mollis arcu fusce nisl voluptate consequat praesent. Veniam morbi nunclorem elementum qui aenean. Nascetur purus litora praesent pede laoreet class minim per. Himenaeos curae cubilia aute fermentum eget. Blandit enim mattis lacus scelerisque morbi per odio nam. Inceptos conubia deserunt vestibulum mi exercitation non ridiculus. Magnis officia aliquip culpa quis fames fringilla vestibulum laborum amet vitae felis. Culpa cum pharetra ea pariatur pede erat tempus massa pulvinar porttitor.
          </p>

          <p>
          Cum cursus suspendisse nibh sodales eiusmod scelerisque vestibulum. Tempor at gravida taciti enim erat magnis. Feugiat curabitur mi aenean placerat dolor suspendisse integer. Adipisicing ullamco commodo tempus ullamcorper dictum massa ligula lorem. Ad vestibulum sapien commodo neque felis consectetur. Laboris ridiculus velit mauris tristique turpis sagittis vehicula sodales irure. Lacinia consectetuer egestas natoque torquent cupidatat pulvinar eu ultricies. Metus tincidunt integer excepteur nunc quis dui odio vestibulum facilisi. Sit pede fames vehicula ultricies.
          </p>

          <p>
          Dolore ultrices dui mi nisi mus laboris consectetur nam nisl sint lobortis. Mattis dictum nostra lorem minim dolor maecenas pharetra. Magnis nisl praesent convallis curae aliquip justo primis pariatur culpa vulputate. Ultricies at aliquip justo pretium. Blandit ultricies natoque vel volutpat non aliquet morbi sunt elit nunclorem aliqua. Integer diam laboris occaecat auctor officia pulvinar tempus. Per do tincidunt est curae condimentum bibendum sunt. Penatibus nunc congue accumsan cras ad velit rhoncus dis. Ea aliquet nam neque ridiculus gravida.
          </p>

          <p>
          Iaculis elementum pharetra vitae pellentesque at. Nibh pulvinar varius quam torquent enim fermentum porttitor morbi dictum sit eiusmod. Venenatis nec sociosqu nam euismod tincidunt occaecat voluptate fugiat aliqua. Esse augue magna odio lobortis sociosqu nam. Porttitor aute do fringilla qui accumsan habitant fugiat occaecat senectus. Litora justo hendrerit lectus sit mollis culpa. Odio venenatis natoque lobortis hendrerit himenaeos. Ligula per habitant inceptos metus. Et nam torquent natoque montes ultrices ante augue. Mi dictum nulla imperdiet non occaecat fusce cras mollis rutrum habitant
          </p>
        </div>
      </div>
    );
  }
}

export default Album;
