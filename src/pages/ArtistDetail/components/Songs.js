import React, { Component } from 'react';
import PropTypes from 'prop-types';

const style = {
  icon: {
    width: 40,
    height: 40,
  },
};

class Item extends Component {
  render() {
    return (
      <tr>
        <td>
          <h4 className="d-inline " >{this.props.index}</h4>
        </td>

        <td>
          <img
            className="img-fluid rounded"
            src={this.props.src}
            style={style.icon}
            alt={this.props.songName}
          />
          <p className="d-inline large text-capitalize ml-1">{this.props.songName}</p>
        </td>

        <td>
          <button type="button" className="btn btn-outline-warning float-right">Play</button>
        </td>
      </tr>
    );
  }
}

Item.propTypes = {
  index: PropTypes.number.isRequired,  
  src: PropTypes.string.isRequired,
  songName: PropTypes.string.isRequired,
};

class ListItem extends Component {
  render() {
    return (
      <table className="table table-striped ">
        <tbody>
          {
            this.props.data.map((song, idx) => (
              <Item key={song.id} index = {idx + 1} src={song.image} songName={song.name}/>
            ))
          }
        </tbody>
      </table>
    );
  }
}

ListItem.propTypes = {
  data: PropTypes.array.isRequired,
};

export default ListItem;