import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { colors } from '../../utils/styles';
import * as actions from '../../actions';
import { login } from '../../utils/model';

const styles = {
  input: {
    marginTop: 10,
  },
  button: {
    backgroundColor: colors.PRIMARY,
    borderRadius: 10,
    borderWidth: 0,
    fontWeight: 'bold',
  },
  heading:{
    textAlign: 'center',
    marginTop: 10,
    marginBottom: 0,
    color: colors.PRIMARY,
    fontWeight: 'bold',
  },
};

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      phone: '',
      password: '',
      isDisplayAlert: false,
    };
  }

  onChange = event => {
    this.setState({ [event.target.name]: event.target.value});
  };

  onClick = () => {
    const { phone, password } = this.state;
    
    // phone: 0978975609, password: admin@123
    login(phone, password)
      .then(response => {
        console.log('Login successfully');
        const tokenId = response.data.token;
        const fullName = response.data.full_name;

        this.props.onLogin(tokenId, fullName);

        this.props.history.push('/');
      })
      .catch(() => {
        this.setState({ isDisplayAlert: true });
      });
  }

  renderAlertError = () => {
    return (
      <div className="alert alert-danger alert-dismissible fade show" role="alert">
        <button type="button" className="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          <span className="sr-only">Close</span>
        </button>
        your username or password is wrong!!!
      </div>
    );
  }

  render() {
    return (
      <div className="container">
        { this.state.isDisplayAlert ? this.renderAlertError() : null }

        <div className="row">
          <div className="col-sm-9 col-md-7 col-lg-5 mx-auto">
            <div className="card card-signin my-5"
              style={{ borderRadius: 15 }}
            >
              <div className='card-heading'>
                <h3 style={styles.heading}>SIGN IN</h3>
              </div>
              <div className="card-body">
                <div className="form-label-group">
                  <input
                    type="number"
                    className="form-control"
                    placeholder="Number phone"
                    style={styles.input}
                    required
                    autoFocus
                    onChange={this.onChange}
                    name='phone'
                    
                  />
                </div>
                <div className="form-label-group">
                  <input
                    type="password"
                    className="form-control"
                    placeholder="Password"
                    required
                    style={styles.input}
                    onChange={this.onChange}
                    name='password'
                    onKeyPress = {e => e.key === 'Enter' ? this.onClick() : null}
                  />
                </div>
                <div className="custom-control custom-checkbox my-3">
                  <input type="checkbox" className="custom-control-input" id="customCheck1" />
                  <label className="custom-control-label" htmlFor="customCheck1">Remember password</label>
                </div>
                <button className="btn btn-lg btn-primary btn-block text-uppercase" type="submit"
                  style={styles.button}
                  onClick={this.onClick}
                >Sign in</button>
                <Link className="btn btn-lg btn-primary btn-block text-uppercase" type="submit"
                  style={styles.button}
                  to="/register"
                >Register</Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Login.propTypes = {
  onLogin: PropTypes.func,
  isLogin: PropTypes.bool,
  tokenId: PropTypes.string,
  fullName: PropTypes.string,
  history: PropTypes.object,
};

const mapStateToProps = state => {
  return {
    isLogin: state.Authentication.isLogin,
    tokenId: state.Authentication.tokenId,
    fullName: state.Authentication.fullName,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onLogin: (tokenId, fullName) => {
      dispatch(actions.login(tokenId, fullName));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
