import React, { Component } from 'react';
import PropTypes from 'prop-types';

import ListItem from '../../Rank/ListItem';
import {  getsongsDES } from '../../../utils/model';
const style = {
  icon: {
    width: 40,
    height: 40,
  },
};

class Title extends Component {
  render() {
    return (
      <div className="p-2">
        <a href="#/" className="text-uppercase small text-white">
          #{this.props.title}
        </a>
      </div>
    );
  }
}

Title.propTypes = {
  title: PropTypes.string.isRequired,
};


class Item extends Component {
  render() {
    return (
      <li className="list-group-item p-1 bg-dark text-white">
        <img
          className="img-fluid float-left"
          src={this.props.src}
          style={style.icon}
          alt={this.props.songName}
        />
        <p className="d-inline small text-capitalize ml-1">{this.props.songName}</p>
        <h4 className="d-inline float-right">{this.props.index}</h4>
      </li>
    );
  }
}

Item.propTypes = {
  src: PropTypes.string.isRequired,
  songName: PropTypes.string.isRequired,
  index: PropTypes.number.isRequired,
};

class WeekChart extends Component {
  constructor(props) {
    super(props);

    this.state = {
      songs: [],
    };

  }
  componentDidMount() {
    getsongsDES()
      .then(response => {
        let arr = response.data;
        for (let i = 0; i < 3; i++) {
          this.setState({
            songs: [
              ...this.state.songs,
              arr[i],
            ],
          });
        }
      })
      .catch(err => {
        console.log(err);
      });
  }
  render() {
    return (
      <div className="row py-2 text-light">
        {/* Zing chart */}
        <div className="col-lg-6 col-md-6">
          <Title title="music today chart" />
          <ListItem data={
            this.state.songs
          }
          />

        </div>

        {/* VietNam Chart */}
        <div className="col-lg-6 col-md-6">
          <Title title="Vietnam chart" />
          <ListItem data={
            this.state.songs
          }
          />
        </div>

        {/* KPop Chart */}
        {/* <div className="col-lg-4 col-md-6">
          <Title title="Kpop chart" />
          <ul className="list-group">
            <Item
              src="https://goo.gl/R8tkSU"
              songName="The hardest part"
              index={1}
            />
            <Item
              src="https://goo.gl/mT8ptx"
              songName="Good bye"
              index={2}
            />
            <Item
              src="https://goo.gl/cMGWsP"
              songName="Siren"
              index={3}
            />
          </ul>
        </div> */}
      </div>
    );
  }
}

export default WeekChart;