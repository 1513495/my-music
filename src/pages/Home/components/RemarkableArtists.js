import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { getArtists } from '../../../utils/model';

const style = {
  divImage: {
    width: 200,
    height: 200,
    borderRadius: 100,
    overflow: 'hidden',
    display: 'inline-block',
  },
  icon: {
    width: 40,
    height: 40,
  },
  title: {
    textTransform: 'capitalize',
    fontSize: 22,
    fontWeight: 'bold',
    marginLeft: 20,
  },
};

class Item extends Component {
  constructor(props) {
    super(props);

    this.state = { isHover: false };
  }

  render() {
    return(
      <div
        className="col-lg-3 col-md-4 col-sm-6 align-items-center justify-content-center mb-3"
        style={{ textAlign: 'center' }}
      >
        {/* avatar */}
        <div style={style.divImage}>
          <Link to="artist">
            <img
              src={this.props.url}
              alt={`avatar of ${this.props.name}`}
              className="mx-auto artist-avatar"
            />
          </Link>
        </div>

        {/* artist name */}
        <div className="mt-2">
          <Link to="artist" className="link">
            <span style={{ textTransform: 'capitalize' }}>
              {this.props.name}
            </span>
          </Link>
        </div>
      </div>
    );
  }
}

Item.propTypes = {
  url: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
};

class RemarkableArtists extends Component {
  constructor(props) {
    super(props);

    this.state = {
      artists: [],
    };
  }

  componentDidMount(){
    getArtists()
      .then(response => {
        let arr = response.data;
        
        let randomNumber = 0;
        for(let i = 0; i < 4; i++){
          randomNumber = Math.floor(Math.random() * (arr.length));

          this.setState({
            artists: [
              ...this.state.artists,
              arr[randomNumber],
            ],
          });

          arr.splice(randomNumber, 1);
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  render() {
    const { artists } = this.state;

    return (
      <div className="row py-2 my-2">
        <div className="col-lg-12">
          {/* title */}
          <Link to="artist" className="link">
            <span style={style.title}>
              nghệ sĩ nổi bật
              &nbsp;
              <i className="fas fa-chevron-right"></i>
            </span>
          </Link>

          {/* list artists */}
          <div className="my-3 row">
            {
              artists.map(artist => (
                <Item key={artist.id} url={artist.options.image} name={artist.full_name}/>
              ))
            }
          </div>
        </div>
      </div>
    );
  }
}

export default RemarkableArtists;