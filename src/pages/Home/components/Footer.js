import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import { colors } from '../../../utils/styles.js';
import '../../../utils/mymusic.scss';

const style = {
  bgBlack: {
    backgroundColor: colors.BLACK_40,
    color: colors.PRIMARY,
  },
  group23: {
    fontSize: 18,
  },
  item: {
    display: 'inline-block',
    width: 120,
    fontSize: 16,
    textTransform: 'capitalize',
  },
};

class Item extends Component {
  render() {
    return (
      <div style={style.item}>
        <Link to={this.props.to} className="link">
          {this.props.content}
        </Link>
      </div>
    );
  }
}

Item.propTypes = {
  to: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
};

class Footer extends Component {
  render() {
    const data = [
      { content: 'Giới thiệu', to: '' },
      { content: 'liên hệ', to: '' },
      { content: 'điều khoản', to: '' },
      { content: 'FAQs', to: '' },
      { content: 'Quảng cáo', to: '' },
      { content: 'góp ý', to: '' },
      { content: 'copyright', to: '' },
    ];

    return (
      <div style={style.bgBlack} className="mt-5 py-3">
        <div className="container">
          <div className="row">
            {/* left */}
            <div className="col-lg-6">
              <div className="d-flex">
                <img
                  src={require('../../../assets/icons/logo-256.png')}
                  style={{ width: 120, height: 120 }}
                  alt=""
                  className="d-block"
                />
                
                <div className="float-left mt-3">
                  <span className="font-weight-bold text-white">My Music</span>
                  <br></br>
                  <span className="font-weight-bold text-white">© 2018 My Music Company</span>
                  <br></br>
                  <span className="text-white" style={style.group23}>Group 23</span>
                </div>
              </div>
            </div>
            
            {/* right */}
            <div className="col-lg-6 my-3">
              <ul className="d-block m-0 px-0">
                {
                  data.map((value, idx) => (
                    <Item key={idx} content={value.content} to={value.to}/>
                  ))
                }
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Footer;