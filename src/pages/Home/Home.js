import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import SliderMain from './components/SliderMain';
import WeekChart from './components/WeekChart';
import Footer from './components/Footer';
import { colors } from '../../utils/styles';
import RemarkableArtists from './components/RemarkableArtists';
// import { getSong } from '../../utils/model';
import * as actions from '../../actions';

class Home extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      artists: [],
    };
  }

  componentDidMount(){
    // getSong(1)
    //   .then(response => {
    //     this.props.addSongToPlayer(response.data);
    //   })
    //   .catch(err => {
    //     console.log(err);
    //   });
  }

  render() {
    return (
      <div>
        <div className="container" style={{ backgroundColor: colors.DARK }}>
          <SliderMain />
          <WeekChart />
          <RemarkableArtists />
        </div>

        <Footer />
      </div>
    );
  }
}

Home.propTypes = {
  addSongToPlayer: PropTypes.func,
};

const mapStateToProps = () => {
  return {

  };
};

const mapDispatchToProps = dispatch => {
  return {
    addSongToPlayer: song => {
      dispatch(actions.addSong(song));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
