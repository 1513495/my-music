import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

// import '../../../utils/mymusic.scss';
// import { colors } from '../../utils/const;


class Video extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isHover: false,
    };
  }
  render() {
    return (
      <div className="card border-primary mb-3" style={{ maxWidth: '18rem' }}>
        <img className="card-img-top" src={this.props.url} alt="Card image cap"
          style={{

            // ...style.image,
            transform: this.state.isHover ? 'scale(1.2, 1.2)' : '',
            // filter: this.state.isHover ? 'grayscale(100%)' : '',
          }
          }
          onMouseEnter={() => { this.setState({ isHover: true }); }}
          onMouseLeave={() => { this.setState({ isHover: false }); }}
          onClick= {()=> {console.log('go to other link');}}
        />
        <div className="card-body text-dark">
          <h6 className="card-title">{this.props.songName}</h6>
          <p className="card-text text-dark" >{this.props.singerName} </p>
          <Link to="album" className="btn btn-warning">Play</Link>
        </div>
      </div>
    );
  }
}

Video.PropTypes = {
  url: PropTypes.string.isRequired,
  songName: PropTypes.string.isRequired,
  singerName: PropTypes.string.isRequired,
};

class RowVideo extends Component {
  render() {
    return (
      <div className="card-deck">
        <Video
          url="https://goo.gl/PWj8hL"
          songName="Giá như anh lặng im"
          singerName="OnlyC, Lou Hoàng, Quang Hùng"
        />

        <Video
          url="https://goo.gl/PWj8hL"
          songName="Giá như anh lặng im"
          singerName="OnlyC, Lou Hoàng, Quang Hùng"
        />
        <Video
          url="https://goo.gl/PWj8hL"
          songName="Giá như anh lặng im"
          singerName="OnlyC, Lou Hoàng, Quang Hùng"
        />
        <Video
          url="https://goo.gl/PWj8hL"
          songName="Giá như anh lặng im"
          singerName="OnlyC, Lou Hoàng, Quang Hùng"
        />

      </div>
    );
  }
}

class ListVideo extends Component {
  render() {
    return (
      <div className="px-5 pt-4">
        <h4 className="text-capitalize">Video</h4>
        <RowVideo />
        <RowVideo />
        <RowVideo />
        <RowVideo />
        <RowVideo />
        <RowVideo />
      </div>
    );
  }
}

export default ListVideo;
