import React, { Component } from 'react';

import ListVideo from './ListVideo';

class Video extends Component {
  render() {
    return (
      <div className="pb-5">
        <div className="container text-white bg-black p-0">
          <ListVideo />
        </div>

      </div>
    );
  }
}

export default Video;
