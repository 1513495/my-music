import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import * as actions from '../../actions';

const style = {
  icon: {
    width: 40,
    height: 40,
  },
};

class Item extends Component {
  render() {
    const { song } = this.props;
    return (
      <tr>
        <td>
          <h4 className="d-inline " >{this.props.index}</h4>
        </td>
        <td>
          <img
            className="img-fluid rounded"
            src={song.image}
            style={style.icon}
            alt={song.name}
          />
          <p className="d-inline large text-capitalize ml-1"
            style={{ cursor: 'pointer' }}
            onClick={() => {
            }}
          >{song.name}</p>
        </td>

        <td>
          <button type="button" className="btn btn-outline-warning float-right"
            onClick={() => {
              this.props.addSong(song);
            }}
          >Play</button>
        </td>
      </tr>
    );
  }
}

Item.propTypes = {
  index: PropTypes.number.isRequired,  
  song: PropTypes.object.isRequired,
  addSong: PropTypes.func.isRequired,
};

class ListItem extends Component {
  render() {
    return (
      <table className="table table-striped ">
        <tbody>
          {
            this.props.data.map((song, idx) => (
              <Item
                key={idx}
                index={idx + 1}
                song={song}
                addSong={this.props.addSong}
              />
            ))
          }
        </tbody>
      </table>
    );
  }
}

ListItem.propTypes = {
  data: PropTypes.array.isRequired,
  addSong: PropTypes.func,
};

const mapStateToProps = state => {
  return {
    isLogin: state.Authentication.isLogin,
    tokenId: state.Authentication.tokenId,
    fullName: state.Authentication.fullName,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    addSong: song => {
      dispatch(actions.addSong(song));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ListItem);