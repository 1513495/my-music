import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import ListItem from './ListItem';
import {  getAlbum } from '../../utils/model';
import * as actions from '../../actions';

class AlbumDetail extends Component {
  
  constructor(props) {
    super(props);

    this.state = {
      name: [],
      link: [],
      image: [],
      songs: [],
    };
  }

  componentDidMount() {
    getAlbum(this.props.albumId)
      .then(response => {
        let obj = response.data;
        
        this.setState({
          name: obj.name,
          link: obj.link,
          image: obj.options.cover,
          songs: obj.songs,
        });
      })
      .catch(err => {
        console.log(err);
      });
  }

  render() {
    return (
      <div className="pb-5">
        <div className="container text-white bg-black p-0">
          <div className="px-5 pt-4">
            <h4 className="text-capitalize my-3">{this.state.name}</h4>

            <ListItem data={this.state.songs}
            />
          </div>
        </div>
      </div>
    );
  }
}

AlbumDetail.propTypes = {
  albumId: PropTypes.string.isRequired,
};

const mapStateToProps = state => {
  return {
    albumId: state.Common.currentAlbumId,
  };
};

const mapDispatchToProps = () => {
  return {

  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AlbumDetail);
