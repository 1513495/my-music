import { combineReducers } from 'redux';

import Authentication from './Authentication';
import Home from './Home';
import Users from './Users';

const myReducer = combineReducers({
  Authentication,
  Home,
  Users,
});

export default myReducer;