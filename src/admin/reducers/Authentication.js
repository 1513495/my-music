import * as types from '../actions/types.js';

const defaultState = {
  isLogin: true,
};

const Authentication = (state = defaultState, action) => {
  switch (action.type){
    case types.LOGIN: {
      return {
        isLogin: true,
      };
    }
    
    default: return state;
  }
};

export default Authentication;