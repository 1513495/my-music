import * as types from '../actions/types.js';

const defaultState = {
  users: [
    {
      phone: '0965075944',
      fullName: 'David',
    },
    {
      phone: '0939893822',
      fullName: 'Tín Nguyễn',
    },
    {
      phone: '0909888291',
      fullName: 'Dark Lord',
    },
  ],
};

const Users = (state = defaultState, action) => {
  switch (action.type){
    case types.FETCH_DATA: {
      return {
      };
    }
    
    default: return state;
  }
};

export default Users;