import * as types from '../actions/types.js';
import { USERS } from '../utils/const';

const defaultState = {
  currentTab: USERS,
};

const Home = (state = defaultState, action) => {
  switch (action.type){
    case types.SET_CURRENT_TAB: {
      if (state.currentTab === action.tab) {
        return state;
      }
      return {
        ...state,
        currentTab: action.tab,
      };
    }

    default: return state;
  }
};

export default Home;