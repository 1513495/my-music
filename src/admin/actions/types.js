export const LOGIN = 'LOGIN';

export const SET_CURRENT_TAB = 'SET_CURRENT_TAB';

export const FETCH_DATA = 'FETCH_DATA';
