import * as types from './types.js';

export const login = () => {
  return {
    type: types.LOGIN,
  };
};