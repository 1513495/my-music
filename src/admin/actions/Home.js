import * as types from './types.js';

export const setCurrentTab = tab => {
  return {
    type: types.SET_CURRENT_TAB,
    tab,
  };
};