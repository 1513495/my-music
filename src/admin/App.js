import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from 'react-router-dom';
import { connect } from 'react-redux';

import Home from './pages/Home';
import Login from './pages/Login';

class App extends Component {
  render() {
    return (
      <div>
        <Router basename='/admin'>
          <div className='container'>
            <Switch>
              <Route path='/login' component={Login} />
              <Route exact path='/' component={Home} />
            </Switch>
          </div>
        </Router>
      </div>
    );
  }
}

App.propTypes = {
};

const mapStateToProps = () => {
  return {
  };
};

const mapDispatchToProps = () => {
  return {
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
