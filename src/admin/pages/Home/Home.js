import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Header from '../../components/Header/Header';
import Users from '../Users';

import { USERS } from '../../utils/const';

class Home extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { currentTab, isLogin, history } = this.props;
    
    if (!isLogin) {
      history.push('/login');
    }
    return (
      <div>
        <Header></Header>
        <div className='row'>
          <div className='col-md-3'>
            <nav className="nav justify-content-left flex-column">
              <a className="nav-link link-black font-weight-bold" href="/admin">Dashboard</a>
              <a className="nav-link link-black font-weight-bold" href="/admin">Users</a>
              <a className="nav-link link-black font-weight-bold" href="/admin">Settings</a>
            </nav>
          </div>
          <div className='col-md-9'>
            {currentTab === USERS ? (<Users></Users>) : null}
          </div>
        </div>
      </div>
    );
  }
}

Home.propTypes = {
  history: PropTypes.object,
  isLogin: PropTypes.bool,
  currentTab: PropTypes.string,
};

const mapStateToProps = state => {
  return {
    isLogin: state.Authentication.isLogin,
    currentTab: state.Home.currentTab,
  };
};

const mapDispatchToProps = () => {
  return {
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);