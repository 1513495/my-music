import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Axios from 'axios';
import { API_URL } from '../../utils/const';


class Users extends Component {
  constructor(props) {
    super(props);

    Axios.get(API_URL + 'song.php')
      .then(response => {
        console.log(response);
      })
      .catch(err => {
        console.log(err);
      });
  }

  render() {
    const { users } = this.props;
    return (
      <div>
        <div className='row'>
          <h3 className='col-12 text-center my-2 font-weight-bold text-warning'>Users</h3>
          <table className="table">
            <thead>
              <tr>
                <th className='text-capitalize'>phone</th>
                <th className='text-capitalize'>name</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {
                users.map((data, idx) => (
                  <tr key={idx}>
                    <td>{data.phone}</td>
                    <td>{data.fullName}</td>
                    <td>
                      <span className='btn btn-sm btn-danger fas fa-eye mx-1'></span>
                      <span className='btn btn-sm btn-danger fas fa-trash-alt mx-1'></span>
                      <span className='btn btn-sm btn-danger mx-1'>
                        <i className="fas fa-redo"></i>
                      </span>
                    </td>
                  </tr>
                ))
              }
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

Users.propTypes = {
  history: PropTypes.object,
  users: PropTypes.array,
};

const mapStateToProps = state => {
  return {
    users: state.Users.users,
  };
};

const mapDispatchToProps = () => {
  return {
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Users);