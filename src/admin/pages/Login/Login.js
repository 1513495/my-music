import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import * as actions from '../../actions';
import { login } from '../../utils/models';

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: '',
    };
  }

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  }

  onLogin = () => {
    const { username, password } = this.state;
    
    login(username, password)
      .then(() => {
        console.log('login admin successfully');
        this.props.onLogin();
      })
      .catch(() => {
        console.log('login admin fail');
      });
  }

  render() {
    if (this.props.isLogin) {
      return (
        <Redirect to='/'></Redirect>
      );
    }

    return (
      <div className='container'>
        <header>
          <h4 className="text-warning font-weight-bold text-center text-uppercase my-5">
            Login Admin Dashboard
          </h4>
        </header>

        {/* login form */}
        <div className='row'>
          <div className='col-lg-8 col-md-10 col-sm-10 mx-auto'>
            {/* username */}
            <div className='form-group'>
              <input
                className="form-control"
                type="text"
                name="username"
                placeholder="Username"
                autoFocus
                onChange={this.onChange}
              />
            </div>
            {/* password */}
            <div className='form-group'>
              <input
                className="form-control"
                type="password"
                name="password"
                placeholder="Password"
                onChange={this.onChange}
                onKeyPress={e => e.key === 'Enter' && this.onLogin() }
              />
            </div>
            {/* button */}
            <div className="text-center">
              <button
                className="btn btn-warning mx-auto text-white font-weight-bold text-uppercase"
                type="submit"
                onClick={this.onLogin}
              >
                Login
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Login.propTypes = {
  onLogin: PropTypes.func,
  isLogin: PropTypes.bool,
};

const mapStateToProps = state => {
  return {
    isLogin: state.Authentication.isLogin,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onLogin: () => {
      dispatch(actions.login());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);