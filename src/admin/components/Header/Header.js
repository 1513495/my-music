import React, { Component } from 'react';
// import {
//   BrowserRouter as Router,
//   Route,
//   Switch,
// } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import NavLink from './NavLink.js';
import { DASHBOARD, USERS, SETTINGS } from '../../utils/const';
import * as actions from '../../actions';
import { colors } from '../../../utils/styles';

class Header extends Component {
  render() {
    const { currentTab } = this.props;
    return (
      <div className='container py-1'>
        <div className='row'>
          <div className='col-md-3 d-flex text-warning font-weight-bold justify-content-between align-items-center'>
            <span
              style={{
                cursor: 'pointer',
              }}
            >My Music</span>
            <span
              style={{
                cursor: 'pointer',
              }}
            >
              <i className="fas fa-bars"></i>
            </span>
          </div>
          <div className='col-md-6 text-warning font-weight-bold mx-0'>
            <NavLink
              value='Dashboard'
              style={{
                color: currentTab === DASHBOARD ? colors.PRIMARY : null,
              }}
              onClick={() => { this.props.setCurrentTab(DASHBOARD); }}
            >
            </NavLink>
            <NavLink
              value='Users'
              style={{
                color: currentTab === USERS ? colors.PRIMARY : null,
              }}
              onClick={() => { this.props.setCurrentTab(USERS); }}
            >
            </NavLink>
            <NavLink
              value='Settings'
              style={{
                color: currentTab === SETTINGS ? colors.PRIMARY : null,
              }}
              onClick={() => { this.props.setCurrentTab(SETTINGS); }}
            >
            </NavLink>
          </div>
          <div className='col-md-3 text-warning font-weight-bold justify-content-end d-flex align-items-center'>
            <i className="fas fa-2x fa-user-circle btn m-0 p-0"></i>
          </div>
        </div>
      </div>
    );
  }
}

Header.propTypes = {
  currentTab: PropTypes.any,
  setCurrentTab: PropTypes.func,
};

const mapStateToProps = state => {
  return {
    currentTab: state.Home.currentTab,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setCurrentTab: tab => {
      dispatch(actions.setCurrentTab(tab));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
