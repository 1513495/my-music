import React, { Component } from 'react';
import PropTypes from 'prop-types';

class NavLink extends Component {
  render() {
    return (
      <div className='d-inline-block mx-1 btn link-black font-weight-bold'
        onClick={this.props.onClick}
        style={this.props.style}
      >
        {this.props.value}
      </div>
    );
  }
}

NavLink.propTypes = {
  value: PropTypes.string,
  onClick: PropTypes.func,
  style: PropTypes.object,
};

export default NavLink;
