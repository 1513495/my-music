import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import TopBar from './components/TopBar';
import MenuBar from './components/MenuBar';
import MusicPlayer from './components/MusicPlayer/MusicPlayer';
import Home from './pages/Home';
import Topic from './pages/Topic';
import Top100 from './pages/Top100';
import Rank from './pages/Rank';
import Artist from './pages/Artist';
import Video from './pages/Video';
import Album from './pages/Album';
import Login from './pages/Login';
import Register from './pages/Register';
import { MUSIC_PLAYER_HEIGHT } from './utils/const';
import AlbumDetail from './pages/AlbumDetail';

import Song from './pages/Song';
import ArtistDetail from './pages/ArtistDetail';

require('./assets/icons/logo.png');

class App extends Component {
  render() {
    const { isDisplayMusicPlayer } = this.props;

    return (
      <div>
        <Router>
          <div className='bg-dark'
            style={{ marginBottom: isDisplayMusicPlayer ? MUSIC_PLAYER_HEIGHT : 0 }}
          >
            <TopBar />
            <MenuBar />
            <Switch>
              <Route path='/rank' component={Rank} />
              <Route path='/top100' component={Top100} />
              <Route path='/topic' component={Topic} />
              <Route path='/video' component={Video} />
              <Route path='/album' component={Album} />
              <Route path='/artist' component={Artist} />
              <Route path='/login' component={Login} />
              <Route path='/register' component={Register} />
              <Route path='/albumDetail' component={AlbumDetail} />
              <Route path='/song' component={Song} />
              <Route path='/artistDetail' component={ArtistDetail} />
              
              <Route exact path='/' component={Home} />
            </Switch>
  
            { isDisplayMusicPlayer ? <MusicPlayer></MusicPlayer> : null }
          </div>
        </Router>
      </div>
    );
  }
}

App.propTypes = {
  isDisplayMusicPlayer: PropTypes.bool,
};

const mapStateToProps = state => {
  return {
    isDisplayMusicPlayer: !state.MusicPlayer.isEmpty,
  };
};

const mapDispatchToProps = () => {
  return {
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
